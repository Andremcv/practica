﻿using System;

namespace Practica

{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio1();
            ejercicio2();
            ejercicio3();
            ejercicio5();
            ejercicio6();
        }
        public static void ejercicio1()
        {
            int numAprobados = 0;
            int numSobresal = 0;
            double nota;

            for (int i = 0; i < 10; i++)
            {
                nota = Double.Parse(Console.ReadLine());
                if (nota >= 9)
                {
                    numSobresal++;
                }
                else if (nota >= 5)
                {
                    numAprobados++;
                }
            }
            Console.WriteLine("El número de aprobados es: " + numAprobados);
            Console.WriteLine("El numero de sobresalientes: " + numSobresal);

        }

        public static void ejercicio2()
        {
            int dia, mes, anio, mesnuevo, aux;
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            anio = Int32.Parse(Console.ReadLine());

            if (dia < 1 || dia > 30 || mes > 12 || mes < 1)
            {
                Console.WriteLine("No has insertado una fecha correctamente");
            }
            else
            {
                mesnuevo = mes + 3;
                if (mesnuevo > 12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                }
                Console.WriteLine("La fehca de pago es: " + dia + "/" + mesnuevo + "/" + anio);
            }

        }

        public static void ejercicio3()
        {
            int[] notas = new int[10];
            notas[0] = 8;
            notas[1] = 1;
            notas[2] = 2;
            notas[3] = 6;
            notas[4] = 3;
            notas[5] = 8;
            notas[6] = 9;
            notas[7] = 0;
            notas[8] = 9;
            notas[9] = 2;

            Array.Sort(notas);
            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine(notas[i]);
            }
        }

        public static void ejercicio5()
        {
            int num1 = 0;
            int num2 = 0;
            int i = 0;
            int x = 0;
            int[] numeros = new int[10];
            numeros[0] = Int32.Parse(Console.ReadLine());
            numeros[1] = Int32.Parse(Console.ReadLine());
            numeros[2] = Int32.Parse(Console.ReadLine());
            numeros[3] = Int32.Parse(Console.ReadLine());
            numeros[4] = Int32.Parse(Console.ReadLine());
            numeros[5] = Int32.Parse(Console.ReadLine());
            numeros[6] = Int32.Parse(Console.ReadLine());
            numeros[7] = Int32.Parse(Console.ReadLine());
            numeros[8] = Int32.Parse(Console.ReadLine());
            numeros[9] = Int32.Parse(Console.ReadLine());
            while (i < 10)
            {
                if (numeros[i] > num1)
                {
                    num1 = numeros[i];
                    x = i;
                }
                i++;
            }
            Array.Clear(numeros, x, 1);
            i = 0;
            while (i < 10)
            {
                if (numeros[i] > num2)
                {
                    num2 = numeros[i];
                }
                i++;
            }
            Console.WriteLine("Los mayores son: " + num1 + " " + num2);
        }


        public static void ejercicio6()
        {
            int[] notas = new int[10];
            notas[0] = 8;
            notas[1] = 1;
            notas[2] = 2;
            notas[3] = 6;
            notas[4] = 3;
            notas[5] = 8;
            notas[6] = 9;
            notas[7] = 0;
            notas[8] = 9;
            notas[9] = 2;

            Array.Sort(notas);
            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine(notas[i]);
            }


            double n1, n2, n3, n4, n5, n6, n7, n8, n9, n10;
            int diferentes = 10;

            n1 = Double.Parse(Console.ReadLine());
            n2 = Double.Parse(Console.ReadLine());
            n3 = Double.Parse(Console.ReadLine());
            n4 = Double.Parse(Console.ReadLine());
            n5 = Double.Parse(Console.ReadLine());
            n6 = Double.Parse(Console.ReadLine());
            n7 = Double.Parse(Console.ReadLine());
            n8 = Double.Parse(Console.ReadLine());
            n9 = Double.Parse(Console.ReadLine());
            n10 = Double.Parse(Console.ReadLine());

            if (n1 == n2 || n1 == n3 || n1 == n4 || n1 == n5 || n1 == n6 || n1 == n7 || n1 == n8 || n1 == n9 || n1 == n10)
            {​​​
                diferentes--;
            }​​​​
            
if (n2 == n1 || n2 == n3 || n2 == n4 || n2 == n5 || n2 == n6 || n2 == n7 || n2 == n8 || n2 == n9 || n2 == n10)
            {​​​​
                diferentes--;
            }​​​​
            
if (n3 == n2 || n1 == n3 || n3 == n4 || n3 == n5 || n3 == n6 || n3 == n7 || n3 == n8 || n3 == n9 || n3 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n4 == n2 || n4 == n3 || n1 == n4 || n4 == n5 || n4 == n6 || n4 == n7 || n4 == n8 || n4 == n9 || n4 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n5 == n2 || n5 == n3 || n5 == n4 || n1 == n5 || n5 == n6 || n5 == n7 || n5 == n8 || n5 == n9 || n5 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n6 == n2 || n6 == n3 || n6 == n4 || n6 == n5 || n1 == n6 || n6 == n7 || n6 == n8 || n6 == n9 || n6 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n7 == n2 || n7 == n3 || n7 == n4 || n7 == n5 || n7 == n6 || n1 == n7 || n7 == n8 || n7 == n9 || n7 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n8 == n2 || n8 == n3 || n8 == n4 || n8 == n5 || n8 == n6 || n8 == n7 || n1 == n8 || n8 == n9 || n8 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n9 == n2 || n9 == n3 || n9 == n4 || n9 == n5 || n9 == n6 || n9 == n7 || n9 == n8 || n1 == n9 || n9 == n10)
            {​​​​
                diferentes--;
            }​​​​
            if (n10 == n2 || n10 == n3 || n10 == n4 || n10 == n5 || n10 == n6 || n10 == n7 || n10 == n8 || n10 == n9 || n1 == n10)
            {​​​​
                diferentes--;

            }​​​​
                Console.WriteLine("Hay estos numeros diferentes " + diferentes);
        }

    }
}
